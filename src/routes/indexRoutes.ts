import  { Router } from 'express';
import indexcontroller from '../controllers/indexController';

class IndexRoutes {

    public router : Router = Router(); 

    constructor(){
        this.config();
    };

    public config (){
        this.router.get('/', indexcontroller.index);
    }
}

export default new IndexRoutes().router;